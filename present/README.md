# Present

Present is a really cool project, but it depends on pyfiglet, WHICH IS A BIG
NO-NO legally speaking T-T...

## Instructions

```
sudo dnf install @fedora-packager
sudo dnf builddep present/present.spec python-asciimatics/python-asciimatics.spec python-pyfiglet/python-pyfiglet.spec
rpmdev-setuptree
spectool -gR present/present.spec python-asciimatics/python-asciimatics.spec python-pyfiglet/python-pyfiglet.spec
rpmbuild -ba present/present.spec python-asciimatics/python-asciimatics.spec python-pyfiglet/python-pyfiglet.spec
```

If you wish to update the software or one of its dependencies:

1. Open the spec file with your favorite editor.
2. Change the version in the line beginning with `Version:`.
4. `spectool -gR VVVVVV.spec`
5. `rpmbuild -ba spec_file.spec`
