%bcond_without	doc

Name:			present
Version:		0.5.1
Release:		1%{?dist}
Summary:		A terminal-based presentation tool with colors and effects

%global forgeurl	https://github.com/vinayak-mehta/present
%global tag			v%{version}

%forgemeta

License:		ASL 2.0
URL:			https://present.readthedocs.io/
Source0:		%{forgesource}

BuildArch:		noarch

BuildRequires:	python3-devel
BuildRequires:	python3-asciimatics
BuildRequires:	python3-click
BuildRequires:	python3-mistune
BuildRequires:	python3-pyfiglet
BuildRequires:	python3-pyyaml

Requires:		python3-asciimatics
Requires:		python3-click
Requires:		python3-mistune
Requires:		python3-pyfiglet
Requires:		python3-pyyaml

%{?python_disable_dependency_generator}

%description
A terminal-based presentation tool with colors and effects.

You can also play a codio (pre-recorded code block) on a slide.

present is built on asciimatics, and it works with Python>=3.7.

Check out the gallery[1] to see what everyone is making with present! You can
add your slides by simply opening an issue.

[1]: https://present.readthedocs.io/en/latest/gallery/index.html


%if %{with doc}
%package		doc
Summary:		Documentation for present

# Doc Requirements
BuildRequires:	python3-sphinx

%description	doc
Additionnal documentation for present.
%endif


%prep
%forgeautosetup


%build
%py3_build

%if %{with doc}
cd docs
make html
%endif

%install
%py3_install


%files
%doc README.md HISTORY.md examples
%license LICENSE
%{_bindir}/%{name}
%{python3_sitelib}/%{name}
%{python3_sitelib}/%{name}-%{version}-py%{python3_version}.egg-info


%if %{with doc}
%files doc
%doc docs/_build/html
%endif


%changelog
* Fri Sep 04 2020 Lyes Saadi <fedora@lyes.eu> - 0.5.1-1
- Initial package
