# Tests fail
%bcond_with tests
%bcond_without doc

%global pypi_name asciimatics

Name:           python-%{pypi_name}
Version:        1.11.0
Release:        1%{?dist}
Summary:        A cross-platform package to replace curses and create ASCII animations

License:        ASL 2.0
URL:            https://github.com/peterbrittain/asciimatics
Source0:        %{pypi_source}
BuildArch:      noarch

BuildRequires:  python3-devel
BuildRequires:  python3dist(future)
BuildRequires:  python3dist(pillow)
BuildRequires:  python3dist(pyfiglet)
BuildRequires:  python3dist(setuptools)
BuildRequires:  python3dist(setuptools-scm)
BuildRequires:  python3dist(wcwidth)

%if %{with tests}
BuildRequires:  python3dist(mock)
# nose is deprecated, using pytest instead.
BuildRequires:  python3dist(pytest)
%endif

%global _description %{expand:
Asciimatics is a package to help people create full-screen text UIs (from
interactive forms to ASCII animations) on any platform. It is licensed under the
Apache Software Foundation License 2.0.

Why?
----

Why not? It brings a little joy to anyone who was programming in the 80s... Oh
and it provides a single cross-platform Python class to do all the low-level
console function you could ask for, including:

- Coloured/styled text - including 256 colour terminals and unicode characters
  (even CJK languages)
- Cursor positioning
- Keyboard input (without blocking or echoing) including unicode support
- Mouse input (terminal permitting)
- Detecting and handling when the console resizes
- Screen scraping

In addition, it provides some simple, high-level APIs to provide more complex
features including:

- Anti-aliased ASCII line-drawing
- Image to ASCII conversion - including JPEG and GIF formats
- Many animation effects - e.g. sprites, particle systems, banners, etc.
- Various widgets for text UIs - e.g. buttons, text boxes, radio buttons, etc.}

%description %{_description}

%package -n     python3-%{pypi_name}
Summary:        %{summary}
%{?python_provide:%python_provide python3-%{pypi_name}}

Requires:       python3dist(future)
Requires:       python3dist(pillow)
Requires:       python3dist(pyfiglet)
Requires:       python3dist(wcwidth)

%description -n python3-%{pypi_name} %{_description}

%if %{with doc}
%package        doc
Summary:        Documentation for asciimatics

BuildRequires:  git
BuildRequires:  python3dist(sphinx)

%description    doc
Additionnal documentation for asciimatics.
%endif

%prep
%autosetup -n %{pypi_name}-%{version}
# Remove bundled egg-info
rm -rf %{pypi_name}.egg-info

%build
%py3_build

%if %{with doc}
git init
git config --global user.email "devel@lists.fedoraproject.org"
git config --global user.name "Fedora Project"
git add .
git commit -m "%{version}"
git tag %{version}
cd doc
./build.sh
mv build html
cd ..
rm -rf .git
%endif

%install
%py3_install

%if %{with tests}
%check
%pytest
%endif

%files -n python3-%{pypi_name}
%license LICENSE
%doc README.rst
%{python3_sitelib}/%{pypi_name}
%{python3_sitelib}/%{pypi_name}-%{version}-py%{python3_version}.egg-info

%if %{with doc}
%files doc
%doc doc/html
%doc doc/source
%endif

%changelog
* Fri Sep 04 2020 Lyes Saadi <fedora@lyes.eu> - 1.11.0-1
- Initial package
