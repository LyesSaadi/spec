#!/bin/sh

COMMIT=$1
SHORTCOMMIT=${COMMIT:0:7}

wget https://github.com/pwaller/pyfiglet/archive/$COMMIT/pyfiglet-$SHORTCOMMIT.tar.gz

tar -xzvf pyfiglet-$SHORTCOMMIT.tar.gz

mv pyfiglet-$COMMIT pyfiglet-$SHORTCOMMIT

rm -rf pyfiglet-$SHORTCOMMIT/pyfiglet/fonts-contrib

tar -czvf pyfiglet-$SHORTCOMMIT-no-contrib-font.tar.gz pyfiglet-$SHORTCOMMIT

# Cleaning
rm -rf pyfiglet-$SHORTCOMMIT/
rm -f pyfiglet-$SHORTCOMMIT.tar.gz
