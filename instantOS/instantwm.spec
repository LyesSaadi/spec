%global forgeurl    https://github.com/instantOS/instantWM
%global tag	    beta2
%forgemeta

Name:           instantwm
Version:        0
Release:        1
Summary:        The window manager for instantOS

License:        MIT
URL:            %{forgeurl}
Source0:        %{forgesource}

BuildRequires:  gcc
BuildRequires:  make
BuildRequires:  libslopy-devel

%description
Placeholder

%prep
%forgesetup

%build
./theme.sh
%set_build_flags
%make_build

%install
install -m 755 -d %{buildroot}%{_bindir}
install -m 755 %{name} %{buildroot}%{_bindir}/%{name}

install -m 755 -d %{buildroot}%{_mandir}/man1
install -m 644 %{name}.1 %{buildroot}%{_mandir}/man1/%{name}.1

ls

install -m 755 -d %{buildroot}%{_datadir}/xsession
#install -m 644 %{name}.desktop %{buildroot}%{_datadir}/xsession/%{name}.desktop

%files
%doc README.md
%license LICENSE
%{_bindir}/%{name}
#%{_datadir}/xsession/%{name}.desktop
%{_mandir}/man1/%{name}.1.gz
