%global shortname Js2Py

%global commit 04241ba58f850cf586c55c25ac033561b9bb82cb
%global shortcommit %(c=%{commit}; echo ${c:0:7})

Name:           python-%{shortname}
Version:        0.67
Release:        1%{?dist}
Summary:        JavaScript to Python Translator & JavaScript interpreter written in 100% pure Python

License:        MIT
URL:            https://github.com/PiotrDabkowski/%{shortname}
Source0:        %{url}/archive/%{commit}/%{name}-%{shortcommit}.tar.gz

BuildArch:      noarch

%global _description %{expand:
Pure Python JavaScript Translator/Interpreter:

Everything is done in 100% pure Python so it's extremely easy to install and
use. Supports Python 2 & 3. Full support for ECMAScript 5.1, ECMA 6 support is
still experimental.

JavaScript 'VirtualMachine' in Python:

A VM-based JavaScript is also implemented. It is feature complete and faster
than the translation based version.}

%description    %{_description}

%package        -n python3-%{shortname}
Summary:        %{Summary}
BuildRequires:  python3-devel
BuildRequires:  python3-pyjsparser
BuildRequires:  python3-six
BuildRequires:  python3-tzlocal
Requires:       python3-pyjsparser
Requires:       python3-six
Requires:       python3-tzlocal
%{?python_provide:%python_provide python3-%{shortname}}

%description    -n python3-%{shortname} %{_description}

%prep
%autosetup      -n %{shortname}-%{commit}

%build
%py3_build

%install
%py3_install

%check
python3 simple_test.py

# These fail...

#PYTHONPATH=`pwd`; export PYTHONPATH;
#cd tests
#python3 run.py
# Changes the test runtime target
#sed -i "18s/RUNTIME_TO_TEST = 'vm'/RUNTIME_TO_TEST = 'translator'/" run.py
#python3 run.py

%files          -n python3-%{shortname}
%license LICENSE.md
%doc README.md examples
%{python3_sitelib}/js2py
%{python3_sitelib}/%{shortname}-%{version}-py*.egg-info

%changelog
* Tue Mar 31 2020 Lyes Saadi <fedora@lyes.eu> - 0.67-1
- Initial package.
