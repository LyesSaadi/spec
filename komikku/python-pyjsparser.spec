%bcond_without bootstrap

%global shortname pyjsparser

%global commit 5465d037b30e334cb0997f2315ec1e451b8ad4c1
%global shortcommit %(c=%{commit}; echo ${c:0:7})

Name:           python-%{shortname}
Version:        2.7.1
Release:        1%{?dist}
Summary:        Fast JavaScript parser for Python

License:        MIT
URL:            https://github.com/PiotrDabkowski/%{shortname}
Source0:        %{url}/archive/%{commit}/%{name}-%{shortcommit}.tar.gz

BuildArch:      noarch

%global _description %{expand:
Fast JavaScript parser - manual translation of esprima.js to python. Takes 1
second to parse whole angular.js library so parsing speed is about 100k
characters per second which makes it the fastest and most comprehensible
JavaScript parser for python out there.

Supports whole ECMAScript 5.1 and parts of ECMAScript 6. The documentation for
the generated AST can be found at:
https://esprima.readthedocs.io/en/3.1/syntax-tree-format.html}

%description    %{_description}

%package        -n python3-%{shortname}
Summary:        %{Summary}
BuildRequires:  python3-devel
%if %{without bootstrap}
BuildRequires:  python3-Js2Py
%endif
%{?python_provide:%python_provide python3-%{shortname}}

%description    -n python3-%{shortname} %{_description}

%prep
%autosetup      -n %{shortname}-%{commit}

%build
%py3_build

%install
%py3_install

%if %{without bootstrap}
%check
python3 test_runner.py
%endif

%files          -n python3-%{shortname}
%license LICENSE
%doc README.md
%{python3_sitelib}/%{shortname}
%{python3_sitelib}/%{shortname}-%{version}-py*.egg-info

%changelog
* Tue Mar 31 2020 Lyes Saadi <fedora@lyes.eu> - 2.7.1-1
- Initial package.
