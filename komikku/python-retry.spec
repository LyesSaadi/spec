%global shortname retry

Name:           python-%{shortname}
Version:        0.9.2
Release:        1%{?dist}
Summary:        Easy to use retry decorator

License:        ASL 2.0
URL:            https://github.com/invl/%{shortname}
Source0:        %{url}/archive/%{version}/%{name}-%{version}.tar.gz
Source1:        tox.ini.fedora

BuildArch:      noarch

%global _description %{expand:
Easy to use retry decorator.

- No external dependency (stdlib only).
- (Optionally) Preserve function signatures (dnf install python3-decorator).
- Original traceback, easy to debug.}

%description    %{_description}

%package        -n python3-%{shortname}
Summary:        %{Summary}
BuildRequires:  git
BuildRequires:  python3-devel
BuildRequires:  python3-mock
BuildRequires:  python3-pbr
BuildRequires:  python3-pytest
BuildRequires:  python3-tox
BuildRequires:  python3-wheel
Recommends:     python3-decorator >= 3.4.2
# There's an error in the requirements.txt, py is not necessary (#19)
%{?python_disable_dependency_generator}
%{?python_provide:%python_provide python3-%{shortname}}

%description    -n python3-%{shortname} %{_description}

%prep
%autosetup      -n %{shortname}-%{version}
# pbr checks for a repo tag in order to generate a version
git init
git add .
git commit -am "Version %{version}"
git tag -am "Version %{version}" %{version}

%build
%py3_build

%install
%py3_install

%check
cp %{SOURCE1} .
tox -c tox.ini.fedora

%files          -n python3-%{shortname}
%license LICENSE
%doc README.rst ChangeLog AUTHORS
%{python3_sitelib}/%{shortname}
%{python3_sitelib}/%{shortname}-%{version}-py*.egg-info

%changelog
* Tue Mar 31 2020 Lyes Saadi <fedora@lyes.eu> - 0.9.2-1
- Initial package.
