%{?mingw_package_header}

%global forgeurl https://github.com/leethomason/tinyxml2

Name:           mingw-tinyxml2
Version:        7.0.1
Release:        1%{?dist}
Summary:        Simple, small and efficient C++ XML parser

%global tag %{version}
%forgemeta

License:        zlib
URL:            %{forgeurl}
Source0:        %{forgesource}
BuildArch:      noarch

BuildRequires:  make
BuildRequires:  cmake

# mingw32 requires
BuildRequires:  mingw32-filesystem
BuildRequires:  mingw32-gcc-c++

# mingw64 requires
BuildRequires:  mingw64-filesystem
BuildRequires:  mingw64-gcc-c++

%global _description %{expand:
TinyXML-2 is a simple, small, efficient, C++ XML parser that can be
easily integrated into other programs. It uses a Document Object Model
(DOM), meaning the XML data is parsed into a C++ objects that can be
browsed and manipulated, and then written to disk or another output stream.

TinyXML-2 doesn't parse or use DTDs (Document Type Definitions) nor XSLs
(eXtensible Stylesheet Language).

TinyXML-2 uses a similar API to TinyXML-1, But the implementation of the
parser was completely re-written to make it more appropriate for use in a
game. It uses less memory, is faster, and uses far fewer memory allocations.}

%description %{_description}

%package        -n mingw32-tinyxml2
Summary:        Simple, small and efficient C++ XML parser

%description -n mingw32-tinyxml2 %{_description}

%package        -n mingw64-tinyxml2
Summary:        Simple, small and efficient C++ XML parser

%description -n mingw64-tinyxml2 %{_description}


%{mingw_debug_package}


%prep
%forgeautosetup
chmod -c -x *.cpp *.h


%build
%mingw_cmake -DBUILD_STATIC_LIBS=OFF
%mingw_make_build


%install
%mingw_make_install


# mingw32
%files -n mingw32-tinyxml2
%doc readme.md
%{mingw32_bindir}/libtinyxml2.dll
%{mingw32_libdir}/libtinyxml2.dll.a
%{mingw32_libdir}/cmake/tinyxml2/tinyxml2*.cmake
%{mingw32_libdir}/pkgconfig/tinyxml2.pc
%{mingw32_includedir}/tinyxml2.h

# mingw64
%files -n mingw64-tinyxml2
%doc readme.md
%{mingw64_bindir}/libtinyxml2.dll
%{mingw64_libdir}/libtinyxml2.dll.a
%{mingw64_libdir}/cmake/tinyxml2/tinyxml2*.cmake
%{mingw64_libdir}/pkgconfig/tinyxml2.pc
%{mingw64_includedir}/tinyxml2.h


%changelog
* Sat May 15 2021 Lyes Saadi <fedora@lyes.eu> - 7.0.1-1
- Initial package
