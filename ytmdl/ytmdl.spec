%global forgeurl https://github.com/deepjyoti30/ytmdl

Name:           ytmdl
Version:        2020.07.09
Release:        1%{?dist}
Summary:        YouTube Music Downloader

%forgemeta

License:        MIT
URL:            %{forgeurl}
Source0:        %{forgesource}

BuildRequires:  python3-devel
BuildRequires:  python3-setuptools

Requires:       youtube-dl
Requires:       python3dist(mutagen)
Requires:       python3dist(itunespy)           # missing
Requires:       python3dist(requests)
Requires:       python3dist(colorama)
Requires:       python3dist(bs4)                # missing
Requires:       python3dist(downloader-cli)     # missing
Requires:       python3dist(lxml)
Requires:       python3dist(pyxdg)
Requires:       python3dist(ffmpeg-python)      # missing
Requires:       python3dist(pysocks)
Requires:       python3dist(tensorflow)         # missing
Requires:       python3dist(inaSpeechSegmenter) # missing

%description
Download songs from YouTube by getting the audio from YouTube and the
metadata from sources like Itunes and Gaana.

This app downloads a song by getting the audio from Youtube sources
using youtube-dl and then adds song information like artist name,
album name, release date, thumbnail etc by fetching it from sources
like Itunes and Gaana.


%prep
%autosetup


%build
%py3_build


%install
%py3_install


%files
%license LICENSE
%doc README.md
%{_bindir}/%{name}
%{python3_sitelib}/%{name}/
%{python3_sitelib}/%{name}-*.egg-info/


%changelog
* Fri Jul 10 2020 Lyes Saadi <fedora@lyes.eu> - 2020.07.09-1
- Initial package.
