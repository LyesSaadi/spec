%global         forgeurl https://github.com/LibreTranslate/LibreTranslate

Name:           libretranslate
Version:        1.2.3
Release:        %autorelease
Summary:        Free and Open Source Machine Translation API built on top of Argos Translate

%forgemeta

# The AGPLv3 license is used for the LibreTranslate project.
# The MIT license is used for these bundled projects:
# - Materialize: materialize.min.css materialize.min.js
# - Prism: prism.min.js
# - Vue.js: vue@2.js
# The Apache2 license is used for Material Icons:
# - CSS: material-icons.css
# - Fonts:
#   - MaterialIcons-Regular.eot
#   - MaterialIcons-Regular.ttf
#   - MaterialIcons-Regular.woff
#   - MaterialIcons-Regular.woff2
License:        AGPLv3 and MIT and Apache2
URL:            %{forgeurl}
Source0:        %{forgesource}

# Licenses
Source10: https://raw.githubusercontent.com/Dogfalo/materialize/master/LICENSE#Materialize-LICENSE
Source11: https://raw.githubusercontent.com/PrismJS/prism/master/LICENSE#Prism-LICENSE
Source12: https://raw.githubusercontent.com/vuejs/vue/dev/LICENSE#Vuejs-LICENSE
Source13: https://raw.githubusercontent.com/google/material-design-icons/master/LICENSE#MaterialIcons-LICENSE

BuildRequires:  python3-devel
BuildRequires:  pyproject-rpm-macros

%description
Free and Open Source Machine Translation API, entirely self-hosted. Unlike other
APIs, it doesn't rely on proprietary providers such as Google or Azure to
perform translations.

%prep
%forgeautosetup

%generate_buildrequires
%pyproject_buildrequires -t

%build
%pyproject_wheel

%install
%pyproject_install

%pyproject_save_files libretranslate

%check
%pytest

%files -f %{pyproject_files}
%license LICENSE %{SOURCE10} %{SOURCE11} %{SOURCE12} %{SOURCE13}
%doc README.md
%{_bindir}/libretranslate
%{_bindir}/ltmanage

%changelog
%autochangelog
