%global forgeurl                          https://gitlab.gnome.org/GNOME/gnome-desktop/
%global gdk_pixbuf2_version               2.36.5
%global gtk4_version                      4.4.0
%global glib2_version                     2.53.0
%global gsettings_desktop_schemas_version 3.27.0
%global po_package                        gnome-desktop-4.0

Name: gnome-desktop4
Version: 41.1
Release: 1%{?dist}
Summary: Library with common API for various GNOME modules

%global commit f18dee1d20f2fb831a9baea462132cb67ee63206

%forgemeta

License: GPLv2+ and LGPLv2+
URL: http://www.gnome.org
Source0: %{forgesource}

BuildRequires: gcc
BuildRequires: gettext
BuildRequires: gtk-doc
BuildRequires: itstool
BuildRequires: meson
BuildRequires: pkgconfig(gdk-pixbuf-2.0) >= %{gdk_pixbuf2_version}
BuildRequires: pkgconfig(gio-2.0) >= %{glib2_version}
BuildRequires: pkgconfig(glib-2.0) >= %{glib2_version}
BuildRequires: pkgconfig(gobject-introspection-1.0)
BuildRequires: pkgconfig(gsettings-desktop-schemas) >= %{gsettings_desktop_schemas_version}
BuildRequires: pkgconfig(gtk4) >= %{gtk4_version}
BuildRequires: pkgconfig(iso-codes)
BuildRequires: pkgconfig(libseccomp)
BuildRequires: pkgconfig(libudev)
BuildRequires: pkgconfig(xkeyboard-config)

Conflicts: shared-mime-info < 2.0-4
Requires: shared-mime-info

%if !0%{?flatpak}
Requires: bubblewrap
%endif
Requires: gdk-pixbuf2%{?_isa} >= %{gdk_pixbuf2_version}
Requires: glib2%{?_isa} >= %{glib2_version}
# needed for GnomeWallClock
Requires: gsettings-desktop-schemas >= %{gsettings_desktop_schemas_version}

# GnomeBGSlideShow API change breaks older gnome-shell versions
Conflicts: gnome-shell < 3.33.4

%description
gnome-desktop contains the libgnome-desktop library as well as a data
file that exports the "GNOME" version to the Settings Details panel.

The libgnome-desktop library provides API shared by several applications
on the desktop, but that cannot live in the platform for various
reasons. There is no API or ABI guarantee, although we are doing our
best to provide stability. Documentation for the API is available with
gtk-doc.

%package devel
Summary: Libraries and headers for %{name}
License: LGPLv2+
Requires: %{name}%{?_isa} = %{version}-%{release}

%description devel
The %{name}-devel package contains libraries and header files for
developing applications that use %{name}.

%prep
%forgeautosetup

%build
%meson -Dlegacy_library=false
%meson_build

%install
%meson_install

rm %{buildroot}/usr/share/gnome/gnome-version.xml

%find_lang %{po_package} --all-name --with-gnome

%files -f %{po_package}.lang
%doc AUTHORS NEWS README.md
%license COPYING COPYING.LIB
%{_libdir}/libgnome-desktop-4.so.{0,1.0.0}
%{_libdir}/libgnome-bg-4.so.{0,1.0.0}
%{_libdir}/libgnome-rr-4.so.{0,1.0.0}
%{_libdir}/girepository-1.0/GnomeDesktop-4.0.typelib
%{_libdir}/girepository-1.0/GnomeBG-4.0.typelib
%{_libdir}/girepository-1.0/GnomeRR-4.0.typelib

%files devel
%{_libdir}/lib*.so
%{_libdir}/pkgconfig/*
/usr/lib/debug/usr/lib64/libgnome-*
%{_includedir}/*
%{_datadir}/gir-1.0/GnomeDesktop-4.0.gir
%{_datadir}/gir-1.0/GnomeBG-4.0.gir
%{_datadir}/gir-1.0/GnomeRR-4.0.gir

%changelog
* Wed Jan 05 2022 Lyes Saadi <mail@lyes.eu> - 41.1-1
- Initial package
