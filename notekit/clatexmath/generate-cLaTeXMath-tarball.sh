#!/bin/sh

COMMIT=$1
SHORTCOMMIT=${COMMIT:0:7}

NAME=cLaTeXMath
OWNER=NanoMichael

ARCHIVE=$NAME-$SHORTCOMMIT

wget https://github.com/$OWNER/$NAME/archive/$COMMIT/$ARCHIVE.tar.gz

tar -xzvf $ARCHIVE.tar.gz

rm $NAME-$COMMIT/res/fonts/maths/special.ttf

tar -czvf $ARCHIVE-clean.tar.gz $NAME-$COMMIT
rm -rf $ARCHIVE.tar.gz $NAME-$COMMIT
