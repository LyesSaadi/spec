Name:           floppy
Version:        0
Release:        1%{?dist}
Summary:        There's no place like ~ !

License:        GPLv3
URL:            https://github.com/manilarome/Glorious-Dotfiles
Source0:        https://github.com/manilarome/Glorious-Dotfiles

Requires:       awesome
Requires:       rofi
Requires:       picom

%description


%prep
%autosetup

%build

%install

%files
%license LICENSE
%doc README.md

%changelog
* Fri Mar 13 2020 Lyes Saadi <fedora@lyes.eu>
- Initial package.
