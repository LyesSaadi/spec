%global commit f7e71bd668fd64d66a57de424c7927a760853ee6
%global shortcommit %(c=%{commit}; echo ${c:0:7})

Name:           VVVVVV
Version:        0
Release:        1.%(date +%%Y%%m%%d)git%{shortcommit}%{?dist}
Summary:        Play VVVVVV: Make and Play edition

License:        Custom and zlib and zlib and zlib and MIT
URL:            https://thelettervsixtim.es/makeandplay/
Source0:        https://github.com/TerryCavanagh/VVVVVV/archive/%{commit}/%{name}-%{shortcommit}.tar.gz
# It is only authorized to ship the Make and Play edition of VVVVVV
Source1:        https://thelettervsixtim.es/makeandplay/data.zip
Source2:        %{name}.desktop
Source3:        %{name}.appdata.xml

BuildRequires:  cmake
BuildRequires:  gcc-c++
BuildRequires:  make
BuildRequires:  SDL2-devel
BuildRequires:  SDL2_mixer-devel
BuildRequires:  unzip

Provides:       bundled(lodepng) == 20191109
Provides:       bundled(physfs) == 3.0.2
Provides:       bundled(tinyxml) == 2.6.2
Provides:       bundled(utf8cpp) == 1.0

%description
The VVVVVV: Make and Play Edition is a completely free, official version of
VVVVVV that includes player levels, and the tools to create levels of your own!
It does not include the original levels from the game.

The commercial version of VVVVVV is still available for purchase at
https://thelettervsixtim.es/, and contains the original levels created by Terry
Cavanagh.

%prep
%autosetup      -n %{name}-%{commit}

%build
cd desktop_version
%cmake .
%make_build

%install
# Installing the binary
install -Dpm 0755 -t %{buildroot}%{_bindir}/ desktop_version/%{name}

# Installing the VVVVVV.desktop and VVVVVV.appdata.xml
install -Dpm 0644 -t %{buildroot}%{_datadir}/applications/ %{SOURCE2}
install -Dpm 0644 -t %{buildroot}%{_metainfodir}/ %{SOURCE3}

# Extracting the data.zip into /usr/share/VVVVVV
unzip %{SOURCE1} -d %{buildroot}%{_datadir}/%{name} && true
# The "true" command at the end is to ignore some unzip complaints because of an old zip bug

%check
desktop-file-validate %{buildroot}%{_datadir}/applications/%{name}.desktop
appstream-util validate-relax --nonet %{buildroot}%{_metainfodir}/%{name}.appdata.xml

%files
%{_bindir}/%{name}
%{_datadir}/%{name}
%{_datadir}/applications/%{name}.desktop
%{_metainfodir}/%{name}.appdata.xml
%license LICENSE.md "License exceptions.md"
%doc README.md

%changelog
* Sun Feb 23 2020 Lyes Saadi <fedora@lyes.eu> - 0-1.20200223gitf7e71bd
- Initial package.
