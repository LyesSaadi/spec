%global pypi_name precise-runner

Name:           python-%{pypi_name}
Version:        0.3.1
Release:        1%{?dist}
Summary:        Wrapper to use Mycroft Precise Wake Word Listener

License:        ASL 2.0
URL:            http://github.com/MycroftAI/mycroft-precise
Source0:        %{pypi_source}
Source1:	https://raw.githubusercontent.com/MycroftAI/mycroft-precise/dev/LICENSE
BuildArch:      noarch

BuildRequires:  python3-devel
BuildRequires:  python3dist(pyaudio)
BuildRequires:  python3dist(setuptools)

%global _description %{expand:
Precise is a wake word listener. The software monitors an audio stream (usually
a microphone) and when it recognizes a specific phrase it triggers an event. For
example, at Mycroft AI the team has trained Precise to recognize the phrase
"Hey, Mycroft". When the software recognizes this phrase it puts the rest of
Mycroft's software into command mode and waits for a command from the person
using the device. Mycroft Precise is fully open source and can be trined to
recognize anything from a name to a cough.

In addition to Precise there are several proprietary wake word listeners out
there. If you are looking to spot a wakeword Precise might be a great solution,
but if it's too resource intensive or isn't accurate enough here are some
alternative options.
}

%description %_description

%package -n     python3-%{pypi_name}
Summary:        %{summary}
%{?python_provide:%python_provide python3-%{pypi_name}}

Requires:       python3dist(pyaudio)

%description -n python3-%{pypi_name} %_description

%prep
%autosetup -n %{pypi_name}-%{version}
# Remove bundled egg-info
rm -rf %{pypi_name}.egg-info
cp %{SOURCE1} .

%build
%py3_build

%install
%py3_install

%check
%{__python3} setup.py test

%files -n python3-%{pypi_name}
%doc README.md
%license LICENSE
%{python3_sitelib}/precise_runner
%{python3_sitelib}/precise_runner-%{version}-py%{python3_version}.egg-info

%changelog
* Sat Jun 06 2020 Lyes Saadi <fedora@lyes.eu> - 0.3.1-1
- Initial Package
