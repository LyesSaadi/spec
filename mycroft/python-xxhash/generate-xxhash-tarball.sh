#!/bin/sh

VERSION=$1

wget https://github.com/ifduyue/python-xxhash/archive/v$VERSION/python-xxhash-$VERSION.tar.gz

tar -xzvf python-xxhash-$VERSION.tar.gz

rm -rf python-xxhash-$VERSION/deps

tar -czvf python-xxhash-$VERSION-no-bundling.tar.gz python-xxhash-$VERSION

# Cleaning
rm -rf python-xxhash-$VERSION/
rm -f python-xxhash-$VERSION.tar.gz
