%global pypi_name xxhash

Name:           python-%{pypi_name}
Version:        1.4.3
Release:        1%{?dist}
Summary:        Python Binding for xxHash

License:        BSD
URL:            https://github.com/ifduyue/python-xxhash
# python-xxhash bundle xxhash sources. To generate a clean tarball:
# ./generate-xxhash-tarball.sh %{version}
Source0:		python-xxhash-%{version}-no-bundling.tar.gz
Source1:		generate-xxhash-tarball.sh

%global _description %{expand:
xxhash is a Python binding for the xxHash library by Yann Collet.
}

%description %_description

%package -n python3-%{pypi_name}
Summary:        %{summary}
Requires:	xxhash-libs
BuildRequires:	gcc
BuildRequires:  python3-devel
BuildRequires:	xxhash-devel

%{?python_provide:%python_provide python3-%{pypi_name}}

%description -n python3-%{pypi_name} %_description

%prep
%forgeautosetup

%build
export XXHASH_LINK_SO=1
%py3_build

%install
%py3_install

%check
export XXHASH_LINK_SO=1
%{python3} setup.py test

%files -n python3-%{pypi_name}
%doc README.rst
%license LICENSE
%{python3_sitearch}/%{pypi_name}/
%{python3_sitearch}/%{pypi_name}-*.egg-info/

%changelog
* Sun Jun  7 2020 Lyes Saadi <fedora@lyes.eu>
- Initial Package
