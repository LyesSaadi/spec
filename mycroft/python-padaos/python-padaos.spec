%global pypi_name padaos

Name:           python-%{pypi_name}
Version:        0.1.10
Release:        1%{?dist}
Summary:        A rigid, lightweight, dead-simple intent parser

License:        MIT
URL:            http://github.com/MatthewScholefield/padaos
Source0:        %{pypi_source}
# Adding the license file as it is not included in the pypi tarball. Issue #7.
Source1:	https://raw.githubusercontent.com/MycroftAI/padaos/master/LICENSE
BuildArch:      noarch

BuildRequires:  python3-devel
BuildRequires:  python3dist(setuptools)

%global _description %{expand:
To deploy an intent parser into production with an automated feedback loop, it's
essential that the new data doesn't interfere with the old data. It's also
necessary that the parser can learn from sentences that closely match written
English (or any other language). That's what Padaos does.
}

%description %_description

%package -n     python3-%{pypi_name}
Summary:        %{summary}
%{?python_provide:%python_provide python3-%{pypi_name}}

%description -n python3-%{pypi_name} %_description

%prep
%autosetup -n %{pypi_name}-%{version}
# Remove bundled egg-info
rm -rf %{pypi_name}.egg-info
cp %{SOURCE1} .

%build
%py3_build

%install
%py3_install

%files -n python3-%{pypi_name}
%doc README.md
%license LICENSE
%{python3_sitelib}/__pycache__/*
%{python3_sitelib}/%{pypi_name}.py
%{python3_sitelib}/%{pypi_name}-%{version}-py%{python3_version}.egg-info

%changelog
* Sat Jun 06 2020 Lyes Saadi <fedora@lyes.eu> - 0.1.10-1
- Initial Package
