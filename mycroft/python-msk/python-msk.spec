%global pypi_name msk

Name:           python-%{pypi_name}
Version:        0.3.16
Release:        1%{?dist}
Summary:        Mycroft Skills Kit

License:        ASL 2.0
URL:            https://github.com/MycroftAI/mycroft-skills-kit
Source0:        %{pypi_source}
BuildArch:      noarch

BuildRequires:  python3-devel
BuildRequires:  python3dist(colorama)
BuildRequires:  python3dist(gitpython)
BuildRequires:  python3dist(msm) >= 0.5.13
BuildRequires:  python3dist(pygithub)
BuildRequires:  python3dist(requests)
BuildRequires:  python3dist(setuptools)

%{?python_disable_dependency_generator}

%global _description %{expand:
A tool to help with creating, uploading, and upgrading Mycroft skills on the
skills repo.
}

%description %_description

%package -n     python3-%{pypi_name}
Summary:        %{summary}
%{?python_provide:%python_provide python3-%{pypi_name}}

Requires:       python3dist(colorama)
Requires:       python3dist(gitpython)
Requires:       python3dist(msm) >= 0.5.13
Requires:       python3dist(pygithub)
Requires:       python3dist(requests)
Requires:       python3dist(setuptools)

%description -n python3-%{pypi_name} %_description

%prep
%autosetup -n %{pypi_name}-%{version}
# Remove bundled egg-info
rm -rf %{pypi_name}.egg-info
cp %{SOURCE1} .

%build
%py3_build

%install
%py3_install

#%%check
#%%{__python3} setup.py test

%files -n       python3-%{pypi_name}
%doc README.md
%license LICENSE
%{_bindir}/%{pypi_name}
%{python3_sitelib}/%{pypi_name}
%{python3_sitelib}/%{pypi_name}-%{version}-py%{python3_version}.egg-info

%changelog 
* Sun Aug 15 2020 Lyes Saadi <fedora@lyes.eu> - 0.3.16-1
- Updating to 0.3.16

* Sat Jun 06 2020 Lyes Saadi <fedora@lyes.eu> - 0.3.15-1
- Initial Package
