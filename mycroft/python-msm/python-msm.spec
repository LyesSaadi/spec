%global pypi_name msm

Name:           python-%{pypi_name}
Version:        0.8.9
Release:        1%{?dist}
Summary:        Mycroft Skills Manager

License:        ASL 2.0
URL:            https://github.com/MycroftAI/mycroft-skills-manager
Source0:        %{pypi_source}
BuildArch:      noarch

# MSM tries to install requirements with sudo if it's not in a venv. Because,
# by default, skills are installed system-wide. This is not the behaviour in
# the RPM-based mycroft, where skills are user-wide, so this will rely on pip
# user installation instead ;).
Patch0:         msm-no-sudo-pip.patch

BuildRequires:  python3-devel
BuildRequires:  python3dist(setuptools)

%global _description %{expand:
Mycroft Skills Manager is a command line tool and a python module for
interacting with the mycroft-skills repository. It allows querying the
repository for information (skill listings, skill meta data, etc) and of course
installing and removing skills from the system.
}

%description %_description

%package -n     python3-%{pypi_name}
Summary:        %{summary}
%{?python_provide:%python_provide python3-%{pypi_name}}

Requires:       python3dist(fasteners)
Requires:       python3dist(gitpython)
Requires:       python3dist(lazy)
Requires:       python3dist(pako)
Requires:       python3dist(pyyaml)
Requires:       python3dist(setuptools)

%description -n python3-%{pypi_name} %_description

%prep
%autosetup -n %{pypi_name}-%{version} -p1
# Remove bundled egg-info
rm -rf %{pypi_name}.egg-info

%build
%py3_build

%install
%py3_install
rm %{buildroot}%{_prefix}/%{pypi_name}/LICENSE

%files -n       python3-%{pypi_name}
%license LICENSE
%{_bindir}/%{pypi_name}
%{python3_sitelib}/%{pypi_name}
%{python3_sitelib}/%{pypi_name}-%{version}-py%{python3_version}.egg-info

%changelog
* Sat Jun 06 2020 Lyes Saadi <fedora@lyes.eu> - 0.8.7-1
- Initial Package
