%global pypi_name fann2

%global tag 1.2.0

Name:           python-%{pypi_name}
Version:        %{tag}
Release:        1%{?dist}
Summary:        Fast Artificial Neural Network Library (FANN) Python bindings

%global forgeurl https://github.com/FutureLinkCorporation/fann2
%forgemeta

License:        LGPLv2+
URL:            %{forgeurl}
Source0:        %{forgesource}

BuildRequires:	fann-devel >= 2.2.0
BuildRequires:	gcc-c++
BuildRequires:	swig

%global _description %{expand:
Python bindings for Fast Artificial Neural Networks 2.2.0 (FANN >= 2.2.0). These
are the original python bindings included with FANN 2.1.0 beta and updated to
include support for python 2.6-3.6.

This is a python binding for Fast Artificial Neural Network Library (FANN >=
2.2.0) that implements multi-layer artificial neural networks with support for
both fully-connected and sparsely-connected networks. It includes a framework
for easily handling training data sets. It is easy to use, versatile,
well-documented, and fast.
}

%description %_description

%package -n python3-%{pypi_name}
Summary:        %{summary}
Requires:	fann >= 2.2.0
BuildRequires:  python3-devel

%{?python_provide:%python_provide python3-%{pypi_name}}

%description -n python3-%{pypi_name} %_description

%prep
%forgeautosetup

%build
%py3_build

%install
%py3_install

%files -n python3-%{pypi_name}
%doc README.rst
%license LICENSE
%{python3_sitearch}/%{pypi_name}/
%{python3_sitearch}/%{pypi_name}-*.egg-info/

%changelog
* Sun Jun  7 2020 Lyes Saadi <fedora@lyes.eu>
- Initial Package
