%global pypi_name padatious

Name:           python-%{pypi_name}
Version:        0.4.8
Release:        1%{?dist}
Summary:        A neural network intent parser

License:        ASL 2.0
URL:            http://github.com/MycroftAI/padatious
Source0:        %{pypi_source}
# Adding the license file as it is not included in the pypi tarball. Issue #14.
Source1:     	https://raw.githubusercontent.com/MycroftAI/padatious/dev/LICENSE
BuildArch:      noarch

BuildRequires:  python3-devel
BuildRequires:  python3dist(setuptools)

%global _description %{expand:
An efficient and agile neural network intent parser. Padatious is a core
component of Mycroft AI.

Features:
- Intents are easy to create
- Requires a relatively small amount of data
- Intents run independent of each other
- Easily extract entities (Find the nearest gas station -> place: gas station)
- Fast training with a modular approach to neural networks
}

%description %_description

%package -n     python3-%{pypi_name}
Summary:        %{summary}
%{?python_provide:%python_provide python3-%{pypi_name}}

Requires:       python3dist(fann2)
Requires:       python3dist(padaos)
Requires:       python3dist(setuptools)
Requires:       python3dist(xxhash)

%description -n python3-%{pypi_name} %_description

%prep
%autosetup -n %{pypi_name}-%{version}
# Remove bundled egg-info
rm -rf %{pypi_name}.egg-info
cp %{SOURCE1} .

%build
%py3_build

%install
%py3_install

%files -n python3-%{pypi_name}
%doc README.md
%license LICENSE
%{_bindir}/padatious
%{python3_sitelib}/%{pypi_name}
%{python3_sitelib}/%{pypi_name}-%{version}-py%{python3_version}.egg-info

%changelog
* Sat Jun 06 2020 Lyes Saadi <fedora@lyes.eu> - 0.4.8-1
- Initial Package
