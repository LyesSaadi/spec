%bcond_with     tests
%bcond_without  doc

Name:           mycroft
Version:        21.2.0
Release:        2%{?dist}
Summary:        Mycroft is a hackable open source voice assistant

%global forgeurl https://github.com/MycroftAI/mycroft-core
%global tag release/v%{version}
%forgemeta

License:        ASL 2.0
URL:            https://mycroft.ai/
Source0:        %{forgesource}

# TODO: Adapt Mycroft commands.

# Mycroft command
Source1:        mycroft

# Mycroft Fedora configuration
Source2:        mycroft.conf

BuildArch:      noarch

%package        core
Summary:        Core package for Mycroft

BuildRequires:  python3-devel
BuildRequires:  python3dist(setuptools)
BuildRequires:  python-unversioned-command

Requires:       mimic

Requires:       mpg123
Requires:       mpg123-plugins-pulseaudio
Requires:       pulseaudio-utils

# Requirements
Requires:       python3dist(six) >= 1.13.0
Requires:       python3dist(requests) >= 2.20.0
Requires:       python3dist(gtts) >= 2.2.2
Requires:       python3dist(pyaudio) >= 0.2.11
Requires:       python3dist(ee) >= 8.1.0
Requires:       python3dist(speech_recognition) >= 3.8.1
Requires:       python3dist(tornado) >= 6.0.3
Requires:       python3dist(websocket-client) >= 0.54.0
Requires:       python3dist(requests-futures) >= 0.9.5
Requires:       python3dist(pyserial) >= 3.0.0
Requires:       python3dist(psutil) >= 5.6.6
Requires:       python3dist(pocketsphinx) < 5
Requires:       python3dist(inflection) >= 0.3.1
Requires:       python3dist(pillow) >= 8.2.0
Requires:       python3dist(dateutil) >= 2.6.0
Requires:       python3dist(fasteners) >= 0.14.1
Requires:       python3dist(pyyaml) >= 5.4.0
Requires:       python3dist(lingua-franca) >= 0.4.2
Requires:       python3dist(msm) >= 0.8.9
Requires:       python3dist(msk) >= 0.3.16
Requires:       python3dist(mycroft-messagebus-client) >= 0.9.1
Requires:       python3dist(adapt-parser) >= 0.5.1
Requires:       python3dist(padatious) >= 0.4.8
Requires:       python3dist(fann2) >= 1.0.7
Requires:       python3dist(padaos) >= 0.1.9
Requires:       python3dist(precise-runner) >= 0.2.1
Requires:       python3dist(petact) >= 0.1.2
Requires:       python3dist(pyxdg) >= 0.26

%{?python_disable_dependency_generator}

%if %{with tests}
# Test Requirements
%endif

# Extra Requirements

%if %{with doc}
%package        doc
Summary:        Documentation for mycroft

# Doc Requirements
BuildRequires:  python3dist(sphinx) >= 2.2.1
BuildRequires:  python3dist(sphinx_rtd_theme) >= 0.4.3
%endif


%description
Mycroft is the world’s first open source voice assistant.

Our software runs on many platforms—on desktop, our Mycroft Mark 1, or on a
Raspberry Pi. This is open source software which can be freely remixed,
extended, and improved. Mycroft may be used in anything from a science project
to an enterprise software application.


%description    core
Core package for Mycroft.


%if %{with doc}
%description    doc
Additionnal documentation for mycroft.
%endif


%prep
%forgeautosetup


%build
%py3_build

%if %{with doc}
cp requirements/requirements.txt .
cd doc
make html
mv _build/html ..
%endif


%install
%py3_install
rm -rf %{buildroot}%{_bindir}/*
cp %{SOURCE1} %{buildroot}%{_bindir}
mkdir -p %{buildroot}%{_sysconfdir}/%{name}
cp %{SOURCE2} %{buildroot}%{_sysconfdir}/%{name}


%if %{with tests}
%check
%endif


%files          core
%doc README.md ACKNOWLEDGEMENTS.md
%license LICENSE.md
%config(noreplace) %{_sysconfdir}/%{name}/%{name}.conf
%{_bindir}/%{name}
%{python3_sitelib}/%{name}
%{python3_sitelib}/%{name}_core-%{version}-py%{python3_version}.egg-info


%if %{with doc}
%files          doc
%doc html
%endif


%changelog
* Sat Aug 14 2020 Lyes Saadi <fedora@lyes.eu>
- Updating to 21.2.0

* Mon Jun 08 2020 Lyes Saadi <fedora@lyes.eu>
- Initial Package
