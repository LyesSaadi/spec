%global pypi_name SpeechRecognition
%global pkg_name speech_recognition

Name:           python-%{pkg_name}
Version:        3.8.1
Release:        1%{?dist}
Summary:        Library for performing speech recognition

License:        BSD
URL:            https://github.com/Uberi/speech_recognition
# SpeechRecognition bundle some binaries/sources. To generate a clean tarball:
# ./generate-SpeechRecognition-tarball.sh 3.8.1
Source0:	speech_recognition-%{version}-no-bundling.tar.gz
Source1:	generate-SpeechRecognition-tarball.sh
BuildArch:      noarch

BuildRequires:  python3-devel
BuildRequires:  python3dist(setuptools)
BuildRequires:  python3dist(pyaudio)
BuildRequires:  python3-pocketsphinx <= 0.3
BuildRequires:	flac

%global _description %{expand:
SpeechRecognition Library for performing speech recognition, with support for
several engines and APIs, online and offline.

Speech recognition engine/API support:
- CMU Sphinx (works offline)
- Google Speech Recognition
- Google Cloud Speech API
- Wit.ai
- Microsoft Bing Voice Recognition
- Houndify API
- IBM Speech to Text
- Snowboy Hotword Detection (works offline)
}

%description %_description

%package -n     python3-%{pkg_name}
Summary:        %{summary}
%{?python_provide:%python_provide python3-%{pypi_name}}

Requires:       python3dist(setuptools)
Requires:       flac


%description -n python3-%{pkg_name} %_description

%prep
%autosetup -n %{pkg_name}-%{version}

%build
%py3_build

%install
%py3_install

%check
export PYTHONPATH=%{buildroot}%{python3_sitelib}
cd tests
# This does nothing? No idea why...
%{python3} test_audio.py

# This test fails... because "one two three" is misunderstood as "wanted to three"...
# %%{python3} test_recognition.py

%files -n python3-%{pkg_name}
%doc README.rst
%license LICENSE.txt
%{python3_sitelib}/%{pkg_name}
%{python3_sitelib}/%{pypi_name}-%{version}-py%{python3_version}.egg-info

%changelog
* Fri Jun 05 2020 Lyes Saadi <fedora@lyes.eu> - 3.8.1-1
- Initial package.
