#!/bin/sh

VERSION=$1

wget https://github.com/Uberi/speech_recognition/archive/$VERSION/speech_recognition-$VERSION.tar.gz

tar -xzvf speech_recognition-$VERSION.tar.gz

rm -f speech_recognition-$VERSION/speech_recognition/flac-*
rm -f speech_recognition-$VERSION/LICENSE-FLAC.txt

rm -rf speech_recognition-$VERSION/third-party
rm -rf speech_recognition-$VERSION/reference

tar -czvf speech_recognition-$VERSION-no-bundling.tar.gz speech_recognition-$VERSION

# Cleaning
rm -rf speech_recognition-$VERSION/
rm -f speech_recognition-$VERSION.tar.gz
