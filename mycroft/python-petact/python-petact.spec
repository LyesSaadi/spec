%global pypi_name petact

Name:           python-%{pypi_name}
Version:        0.1.2
Release:        1%{?dist}
Summary:        A package extraction tool

License:        MIT
URL:            https://github.com/matthewscholefield/petact
Source0:        %{pypi_source}
# Adding the license file as it is not included in the pypi tarball.
Source1:	https://raw.githubusercontent.com/MatthewScholefield/petact/master/LICENSE
BuildArch:      noarch

BuildRequires:  python3-devel
BuildRequires:  python3dist(setuptools)

%global _description %{expand:
Petact is a library used for installing and updating compressed tar files. When
install_package is called, it downloads an md5 file and compares it with the md5
of the locally downloaded tar. If they are different, the old extracted files
are deleted and the new tar is downloaded and extracted to the same place.
}

%description %_description

%package -n     python3-%{pypi_name}
Summary:        %{summary}
%{?python_provide:%python_provide python3-%{pypi_name}}

Requires:       python3dist(setuptools)

%description -n python3-%{pypi_name} %_description

%prep
%autosetup -n %{pypi_name}-%{version}
# Remove bundled egg-info
rm -rf %{pypi_name}.egg-info
cp %{SOURCE1} .

%build
%py3_build

%install
%py3_install

%files -n python3-%{pypi_name}
%doc README.md
%license LICENSE
%{_bindir}/petact
%{python3_sitelib}/%{pypi_name}
%{python3_sitelib}/%{pypi_name}-%{version}-py%{python3_version}.egg-info

%changelog
* Sat Jun 06 2020 Lyes Saadi <fedora@lyes.eu> - 0.1.2-1
- Initial Package
