%global pypi_name pocketsphinx

Name:           python-%{pypi_name}
Version:        0.1.15
Release:        1%{?dist}
Summary:        Python interface to CMU Sphinxbase and Pocketsphinx libraries

License:        BSD
URL:            https://github.com/bambocher/pocketsphinx-python
Source0:        %{pypi_source}

BuildRequires:  python3-devel
BuildRequires:  python3dist(setuptools)

BuildRequires:	alsa-lib-devel
BuildRequires:	gcc-c++
BuildRequires:	pulseaudio-libs-devel
BuildRequires:	swig

%global _description %{expand:
Pocketsphinx is a part of the CMU Sphinx Open Source Toolkit For Speech
Recognition.

This package provides a python interface to CMU Sphinxbase and Pocketsphinx
libraries created with SWIG and Setuptools.
}

%description %_description

%package -n     python3-%{pypi_name}
Summary:        %{summary}
%{?python_provide:%python_provide python3-%{pypi_name}}

Provides:       bundled(pocketsphinx) == c178c8dc1948685ed93c6c4ee93122a7bc789cfd
Provides:       bundled(sphinxbase) == a74a11df3a021e9a26b0d20c3de999b8eb0afcef

%description -n python3-%{pypi_name} %_description

%prep
%autosetup -n %{pypi_name}-%{version}
# Remove bundled egg-info
rm -rf %{pypi_name}.egg-info

%build
%set_build_flags
%py3_build

%install
%py3_install

%files -n python3-%{pypi_name}
%license LICENSE
%doc README.md
%{python3_sitearch}/%{pypi_name}
%{python3_sitearch}/sphinxbase
%{python3_sitearch}/%{pypi_name}-%{version}-py%{python3_version}.egg-info

%changelog
* Sat Jun 06 2020 Lyes Saadi <fedora@lyes.eu> - 0.1.15-1
- Initial Package
