%global pypi_name lazy

Name:           python-%{pypi_name}
Version:        1.4
Release:        1%{?dist}
Summary:        Lazy attributes for Python objects

License:        BSD
URL:            https://github.com/stefanholek/lazy
Source0:        %{pypi_source %{pypi_name} %{version} zip}
BuildArch:      noarch

BuildRequires:  python3-devel
BuildRequires:  python3dist(setuptools)

%global _description %{expand:
A decorator to create lazy attributes.

Lazy attributes are computed attributes that are evaluated only once, the first
time they are used. Subsequent uses return the results of the first call. They
come handy when code should run:

- late: i.e. just before it is needed.
- once: i.e. not twice, in the lifetime of an object.

You can think of it as deferred initialization. The possibilities are endless.
}

%description %_description

%package -n     python3-%{pypi_name}
Summary:        %{summary}
%{?python_provide:%python_provide python3-%{pypi_name}}

%description -n python3-%{pypi_name} %_description

%prep
%autosetup -n %{pypi_name}-%{version}
# Remove bundled egg-info
rm -rf %{pypi_name}.egg-info

%build
%py3_build

%install
%py3_install

%check
%{__python3} setup.py test

%files -n python3-%{pypi_name}
%license LICENSE
%doc README.rst
%{python3_sitelib}/%{pypi_name}
%{python3_sitelib}/%{pypi_name}-%{version}-py%{python3_version}.egg-info

%changelog
* Sat Jun 06 2020 Lyes Saadi <fedora@lyes.eu> - 1.4-1
- Initial Package
