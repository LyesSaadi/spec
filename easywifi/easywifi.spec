%global commit a124e4a60e5b0789bf97ec5a7f478e091e4a1b8b
%global shortcommit %(c=%{commit}; echo ${c:0:7})

Name:           easywifi
Version:        0
Release:        3.%(date +%%Y%%m%%d)git%{shortcommit}%{?dist}
Summary:        The easiest way to add WiFi networks on linux

License:        MIT
URL:            https://github.com/NoahCristino/easywifi/
Source0:        https://github.com/NoahCristino/easywifi/archive/%{commit}/%{name}-%{shortcommit}.tar.gz

BuildArch:      noarch

Requires:       NetworkManager
Requires:       python3

Recommends:     dnsmasq

%description
easywifi speeds up the time it takes to add new WiFi connections and
to switch between them, using nmcli.

Features:
* Scan for WiFi networks
* List network devices
* List saved network profiles
* Connect to saved networks
* Setup new networks
* Creation of hotspots

%prep
%setup -qn %{name}-%{commit}
sed -i "1i#!/usr/bin/python3" easywifi.py
mv easywifi.py easywifi

%build

%install
install -Dpm 0755 -t %{buildroot}%{_bindir}/ easywifi

%files
%{_bindir}/%{name}
%license LICENSE
%doc README.md

%changelog
* Wed Jan 1 2020 Lyes Saadi <fedora@lyes.eu> - 0-3.20200101gita124e4a
- Another dumb mistake (sbin -> bin this time).

* Wed Jan 1 2020 Lyes Saadi <fedora@lyes.eu> - 0-2.20200101gita124e4a
- Fix dumb Require mistake.

* Wed Jan 1 2020 Lyes Saadi <fedora@lyes.eu> - 0-1.20200101gita124e4a
- Initial package.
