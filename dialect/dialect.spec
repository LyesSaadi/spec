%global         uuid com.github.gi_lom.dialect
%global         forgeurl https://github.com/gi-lom/dialect

Name:           dialect
Version:        1.0.0
Release:        1%{?dist}
Summary:        A translation app for GNOME based on Google Translate

%global         tag %{version}
%forgemeta

License:        GPLv3+
URL:            %{forgeurl}
Source0:        %{forgesource}

BuildArch:      noarch

BuildRequires:  meson
BuildRequires:  desktop-file-utils
BuildRequires:  libappstream-glib
BuildRequires:  gstreamer1-devel
BuildRequires:  gtk3-devel
BuildRequires:  libhandy1-devel
BuildRequires:  python3-devel
BuildRequires:  python3-gobject-devel
BuildRequires:  python3dist(googletrans)
BuildRequires:  python3dist(gtts)

Requires:       hicolor-icon-theme
Requires:       gstreamer1-plugins-base
Requires:       gtk3
Requires:       libhandy1
Requires:       python3-gobject
Requires:       python3dist(googletrans)
Requires:       python3dist(gtts)


%description
A translation app for GNOME based on Google Translate.

Features:
* Text translation up to 5000 chars
* Text to speech
* History
* Automatic language detection
* Clipboard buttons


%prep
%forgeautosetup


%build
%meson
%meson_build


%install
%meson_install

%find_lang %{name}


%check
desktop-file-validate %{buildroot}/%{_datadir}/applications/%{uuid}.desktop
appstream-util validate-relax --nonet %{buildroot}%{_metainfodir}/%{uuid}.metainfo.xml


%files -f %{name}.lang
%license LICENSE
%doc README.md
%{_bindir}/%{name}
%{_datadir}/applications/%{uuid}.desktop
%{_datadir}/%{name}
%{_datadir}/glib-2.0/schemas/%{uuid}.gschema.xml
%{_datadir}/icons/hicolor/scalable/apps/%{uuid}*.svg
%{_metainfodir}/%{uuid}.metainfo.xml


%changelog
* Thu Oct 15 2020 Lyes Saadi <fedora@lyes.eu> - 1.0.0-1
- Initial package
