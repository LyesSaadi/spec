#!/bin/sh

VERSION=$1

tar -xzvf gitlab-$VERSION.tar.gz

rm -rf gitlab-$VERSION/ee

tar -czvf gitlab-$VERSION-ce.tar.gz gitlab-$VERSION