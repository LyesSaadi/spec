Name:           gitlab
Version:        12.9.0
Release:        1%{?dist}
Summary:        summary

License:        MIT
URL:            https://about.gitlab.com/
# https://gitlab.com/OWNER/PROJECT/-/archive/v%%{version}-ee/%%{name}-%%{version}.tar.gz
Source0:        %{name}-%{version}-ce.tar.gz
# GitLab ee is licensed under a proprietary license, we cannot, therefore,
# include it in Fedora repositories, and no right is given to redistribute the
# source code.
# Download the upstream tarball and invoke this script while in the
# tarball's directory:
# ./generate-tarball.sh %%{version}
Source1:        generate-tarball.sh

#BuildRequires:  test
#Requires:       test

%description
description

%prep
%autosetup

%build
%configure
%make_build

%install
%make_install

%files
%license add-license-file-here
%doc add-docs-here

%changelog
* Sun Mar 22 2020 Lyes Saadi <fedora@lyes.eu>
- Initial package.
