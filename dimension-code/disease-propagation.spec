%global commit cd81dd2bc3c89a02aad8f8979c99cdb76c614d63
%global shortcommit %(c=%{commit}; echo ${c:0:7})

%global reponame Disease_propagation

Name:           disease-propagation
Version:        1.1
Release:        1.%(date +%%Y%%m%%d)git%{shortcommit}%{?dist}
Summary:        Basic simulation of disease propagation

License:        CC0
URL:            https://www.youtube.com/watch?v=hrLrEfP2Wjo
Source0:        https://github.com/angeluriot/%{reponame}/archive/%{commit}/%{name}-%{shortcommit}.tar.gz
Source1:        disease-propagation.desktop
Source2:        https://creativecommons.org/publicdomain/zero/1.0/legalcode.txt

Patch0:         cmake-remove-sfml-main.patch
Patch1:         disease-propagation-data-path.patch
Patch2:         disease-propagation-icon-path.patch
Patch4:         disease-propagation-add-math.patch
Patch5:         disease-propagation-fix-accents.patch
Patch6:         disease-propagation-fix-types.patch
Patch7:         disease-propagation-fix-types-headers.patch

BuildRequires:  cmake
BuildRequires:  gcc-c++
BuildRequires:  make
BuildRequires:  SFML-devel

%description
This is a project from the french Youtuber Dimension, where he tries to
simulate the propagation of a disease.

The rule of the disease propagation are 100% customizable from the integrated
menu. Where you can change things like:
* Percentage of vaccinated
* Lethality of the disease
* Infectivity of the disease
* And more...

%prep
%autosetup      -p0 -n %{reponame}-%{commit}

%build
rm -rf dependencies/SFML
# Convert every file from ISO-8859-1 to UTF-8
find . -type f \! -name "*.png" \! -name "*.ttf" -exec iconv -f ISO-8859-1 -t UTF-8 {} -o {} \; -exec basename {} \;
%cmake .
%make_build

# Rename the binary to a more common name
mv Disease_propagation disease-propagation

# Getting the license
cp -p %{SOURCE2} .

%install
# Installing the binary
install -Dpm 0755 -t %{buildroot}%{_bindir}/ disease-propagation

# Installing resources
install -Dpm 0644 -t %{buildroot}%{_datadir}/%{name} dependencies/resources/*

# Installing desktop file
install -Dpm 0644 -t %{buildroot}%{_datadir}/applications %{SOURCE1}

%files
%{_bindir}/disease-propagation
%{_datadir}/%{name}
%{_datadir}/applications/%{name}.desktop
%doc README.md
%license legalcode.txt

%changelog
* Sun Feb 23 2020 Lyes Saadi <fedora@lyes.eu> - 1.1-1.20200308gitcd81dd2
- Update to v1.1
- Now, the program work on all screens.
- Fix encoding.
- Including CC0 License.

* Sat Feb 22 2020 Lyes Saadi <fedora@lyes.eu> - 0-1.20200222gitdf9ee40
- Initial package.
